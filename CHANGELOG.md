# Changelog
## V0.5.3
**Enhancements**
- Place all settings in settings.php
- Added manual

**Bug fixes**
- Fixed wrong mysql statement

## V0.5.2
**Important changes**
- Be ready to make it public(Removed all test information)
- Some changes on output format of output.php

**Enhancements**
- Added foodtype calculation on output.php

**Project general upgrades**
- Updated LICENSE
- Updated Changelog
- Updated Readme

## V0.5.1
**Enhancements**
- Added summary views

**Bug fixes**
- Fixed the miscount on output.php

## V0.5.0
- Initial commit with a basic bone