﻿# Mysql Statements
## Why configure it manually
Because I don't have too much time to write a automatic script

## Before use (GLOBAL)
Excute
```sql
CREATE TABLE `res` (
    `resid` VARCHAR(32) NOT NULL,
    `address` MEDIUMTEXT NULL,
    `name` MEDIUMTEXT NULL,
    `latitude` VARCHAR(16) NULL,
    `longitude` VARCHAR(16) NULL,
    `ordernum` INT NULL,
    `date` VARCHAR(10) NULL,
    PRIMARY KEY (`resid`),
    FULLTEXT INDEX `ADDRESS` (`address` ASC,`name` ASC))
  ENGINE = MyISAM
  COMMENT = 'For Restaurant data collecting';
CREATE TABLE `food` (
  `itemid` VARCHAR(32) NOT NULL,
  `resid` VARCHAR(32) NOT NULL,
  `name` MEDIUMTEXT NULL,
  `description` MEDIUMTEXT NULL,
  `type` CHAR(1) NULL,
  `monthsale` INT NULL,
  `date` VARCHAR(10) NULL,
  PRIMARY KEY (`itemid`),
  INDEX RESINDEX (`resid`),
  FULLTEXT INDEX `INDEX` (`name` ASC))
ENGINE = MyISAM
COMMENT = 'For food collecting';
```

## Before processing word
**IMPORTANT!!!**
**DONNOT** excute it before archiving items
```sql
create table foodtype select * from food;
ALTER TABLE `foodtype`
ENGINE = MyISAM ,
ADD COLUMN `internalid` INT(32) UNSIGNED NOT NULL AUTO_INCREMENT FIRST,
ADD COLUMN `status` CHAR(1) NULL AFTER `date`,
ADD COLUMN `foodtype` VARCHAR(1) NULL AFTER `status`,
ADD COLUMN `suittype` MEDIUMTEXT NULL AFTER `foodtype`,
ADD COLUMN `value` CHAR(6) NULL AFTER `suittype`,
ADD PRIMARY KEY (`internalid`);
```
