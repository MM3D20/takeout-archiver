﻿<?php
require_once 'settings.php';
$DB_SERVER = mysqli_connect($db_host, $db_user, $db_pass, $db_name) or die('DB ERROR');
$extras = 'extras%5B%5D='.'activities';
$limit = 'limit='.'24';
$offset = 0;
$terminal = 'terminal='.'web';
$HEADERS[] = 'Accept: */*';
$HEADERS[] = 'Connection: Keep-Alive';
$HEADERS[] = 'Content-type: application/x-www-form-urlencoded;charset=UTF-8';
$REQ_INTERVAL = 5;

/*  EXCUTE FOLLOWING STATEMENTS IN MYSQL IN ORDER TO CONFIGURE DATABASE

CREATE TABLE `res` (
    `resid` VARCHAR(32) NOT NULL,
    `address` MEDIUMTEXT NULL,
    `name` MEDIUMTEXT NULL,
    `latitude` VARCHAR(16) NULL,
    `longitude` VARCHAR(16) NULL,
    `ordernum` INT NULL,
    `date` VARCHAR(10) NULL,
    PRIMARY KEY (`resid`),
    FULLTEXT INDEX `ADDRESS` (`address` ASC,`name` ASC))
  ENGINE = MyISAM
  COMMENT = 'For Restaurant data collecting';

CREATE TABLE `food` (
  `itemid` VARCHAR(32) NOT NULL,
  `resid` VARCHAR(32) NOT NULL,
  `name` MEDIUMTEXT NULL,
  `description` MEDIUMTEXT NULL,
  `type` CHAR(1) NULL,
  `monthsale` INT NULL,
  `date` VARCHAR(10) NULL,
  PRIMARY KEY (`itemid`),
  INDEX RESINDEX (`resid`),
  FULLTEXT INDEX `INDEX` (`name` ASC))
ENGINE = MyISAM
COMMENT = 'For food collecting';

*/
function db_query($query)
{
    global $DB_SERVER;
    return mysqli_query($DB_SERVER, $query);
}
function food_write($item_id, $res_id, $name, $description, $type, $monthsale)
{
    $cmp = db_query("SELECT * FROM food WHERE itemid='$item_id' AND resid='$res_id' LIMIT 1");
    if (mysqli_fetch_array($cmp) == false) {
        $time = time();
        db_query("INSERT INTO food VALUES('$item_id','$res_id','$name','$description','$type','$monthsale','$time')");
        echo '[INFO]['.$res_id.']'.' FoodID: '.$item_id.'  Fetched.'.PHP_EOL;
    }
}
function res_write($res_id, $address, $name, $latitude, $longitude, $monthsale)
{
    $cmp = db_query("SELECT * FROM res WHERE resid='$res_id' LIMIT 1");
    if (mysqli_fetch_array($cmp) == false) {
        $time = time();
        db_query("INSERT INTO res VALUES('$res_id','$address','$name','$latitude','$longitude','$monthsale','$time')");
        echo '[INFO] ResID: '.$res_id.'  Fetched.'.PHP_EOL;
    }
}
function process_resid($resid)
{
    global $REQ_INTERVAL;
    global $UA;
    global $HEADERS;
    unset($ch);
    $url = 'https://www.ele.me/restapi/shopping/v2/menu?restaurant_id='.$resid;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_USERAGENT, $UA);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $HEADERS);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $res_string = curl_exec($ch);
    curl_close($ch);
    $res_json = json_decode($res_string, true);
    /* /for json prasing
        * array[]
        *     foods
        *         array[]
        *             name
        *             month_sales
        *             description
        * NOTE:IN MAIN ARRAY TYPE
        *            1-retail==>get this
        *            2-promo
        *            3-deal
        * eg:echo $json[0]['foods'][0]['name'];
    */
    foreach ($res_json as $section) {
        if ($section['type']==1) {
            foreach ($section['foods'] as $foods) {
                food_write($foods['item_id'], $foods['restaurant_id'], $foods['name'], $foods['description'], 1, $foods['month_sales']);
            }
        }
    }
    unset($res_json);
    unset($res_string);
    sleep($REQ_INTERVAL);
}


do {
    unset($string);
    unset($json);
    $url = 'https://www.ele.me/restapi/shopping/restaurants?'.$extras.'&'.$geohash.'&'.$latitude.'&'.$limit.'&'.$longitude.'&offset='.$offset.'&'.$terminal;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_USERAGENT, $UA);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $HEADERS);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $string = curl_exec($ch);
    curl_close($ch);
    unset($ch);
    $json = json_decode($string, true);
    /* /for json prasing
        * array[]
        *     id
        *     name
        *     recent_order_num
        *     address
        * eg:echo $json[0]['id'];
    */
    if (!empty($json)) {
        foreach ($json as $section) {
            res_write($section['id'], $section['address'], $section['name'], $section['latitude'], $section['longitude'], $section['recent_order_num']);
            process_resid($section['id']);
        }
    }
    $offset += 24;
    sleep($REQ_INTERVAL);
    // security code
    if ($offset>=12000) {
        echo '[WARN] Restaurant request limit exceed,ABORTING.';
        break;
    }
} while (!empty($json));
