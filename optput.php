<?php
require_once 'settings.php';
$db_server = mysqli_connect($db_host, $db_user, $db_pass, $db_name);
// Define all variables
// Counter for total mounthsale invalid sale and valid sale(parsed)
$cnt_t = 0;
$cnt_ti = 0;
$cnt_tv = 0;
// The counter for single item
$s_chop = 0;
$s_pbag = 0;
$s_pbowl = 0;
$s_af = 0;
$s_p = 0;
// The counter for suit
$su_chop = 0;
$su_pbag = 0;
$su_pbowl = 0;
$su_af = 0;
$su_p =0;
// The counter for single items and suit items
$s_count = 0;
$s_num = 0;
$su_count = 0;
$su_num = 0;
// Foodtypes
$rice = 0;
$noodles = 0;
$hamburger = 0;
$friedmeat = 0;
$dumplings = 0;
$dishes = 0;
$drinks = 0;
$soup = 0;
$bakes = 0;

function db_query($query)
{
    global $db_server;
    return mysqli_query($db_server, $query);
}
$result = db_query("SELECT COUNT(*) FROM foodtype");
$counts = mysqli_fetch_row($result)[0];
unset($result);
for ($i=1; $i<=$counts; $i++) {
    $result = db_query("SELECT * FROM eleme.foodtype WHERE internalid='$i'");
    $row = mysqli_fetch_row($result);
    if ($row[8]==1) {
        $cnt_t += $row[6];
        $cnt_tv += $row[6];
        if ($row[9] != null) {
            $s_count ++;
            $s_num += $row[6];
            $tmp = str_split($row[11]);
            $s_chop += (int)$tmp[0]*$row[6];
            $s_pbag += (int)$tmp[1]*$row[6];
            $s_pbowl += (int)$tmp[2]*$row[6];
            $s_af += (int)$tmp[3]*$row[6];
            $s_p += (int)$tmp[4]*$row[6];
        } elseif ($row[10] != null) {
            $su_count ++;
            $su_num += $row[6];
            $tmp = str_split($row[11]);
            $su_chop += (int)$tmp[0]*$row[6];
            $su_pbag += (int)$tmp[1]*$row[6];
            $su_pbowl += (int)$tmp[2]*$row[6];
            $su_af += (int)$tmp[3]*$row[6];
            $su_p += (int)$tmp[4]*$row[6];
        }
        switch ($row[9]) {
            case 1:
                $rice += $row[6];
                break;
            case 2:
                $noodles += $row[6];
                break;
            case 3:
                $hamburger += $row[6];
                break;
            case 4:
                $friedmeat += $row[6];
                break;
            case 5:
                $dumplings += $row[6];
                break;
            case 6:
                $dishes += $row[6];
                break;
            case 7:
                $drinks += $row[6];
                break;
            case 8:
                $soup += $row[6];
                break;
            case 9:
                $bakes += $row[6];
                break;
        }
    } else {
        $cnt_t += $row[6];
        $cnt_ti += $row[6];
    }
}
$t_chop = $su_chop+$s_chop;
$t_pbag = $su_pbag+$s_pbag;
$t_pbowl = $su_pbowl+$s_pbowl;
$t_af = $su_af+$s_af;
$t_p = $su_p+$s_p;
echo 'Summary:'.PHP_EOL.$counts.' rows with monthsale '.$cnt_t.PHP_EOL;
echo 'Parsed '.$cnt_tv.' sales. Not parsed '.$cnt_ti.' sales.'.PHP_EOL;
echo 'Result:'.PHP_EOL.'For Single: Chopsticks : '.$s_chop.' Plastic Bag : '.$s_pbag.' Plastic Bowl : '.$s_pbowl.' Aluminium fuil : '.$s_af.' Paper : '. $s_p;
echo PHP_EOL.'For Suit: Chopsticks : '.$su_chop.' Plastic Bag : '.$su_pbag.' Plastic Bowl : '.$su_pbowl.' Aluminium fuil : '.$su_af.' Paper : '. $su_p;
echo PHP_EOL.'Total: Chopsticks : '.$t_chop.' Plastic Bag : '.$t_pbag.' Plastic Bowl : '.$t_pbowl.' Aluminium fuil : '.$t_af.' Paper : '.$t_p;
echo PHP_EOL.'Proceed '.$s_count.' single items. With monthsale '.$s_num;
echo PHP_EOL.'Proceed '.$su_count.' suit items. With monthsale '.$su_num;
echo PHP_EOL.'Proceed '.$rice.' monthsales with rice.';
echo PHP_EOL.'Proceed '.$noodles.' monthsales with noodles.';
echo PHP_EOL.'Proceed '.$hamburger.' monthsales with hamburger.';
echo PHP_EOL.'Proceed '.$friedmeat.' monthsales with friedmeat.';
echo PHP_EOL.'Proceed '.$dumplings.' monthsales with dumplings.';
echo PHP_EOL.'Proceed '.$dishes.' monthsales with dishes.';
echo PHP_EOL.'Proceed '.$drinks.' monthsales with drinks.';
echo PHP_EOL.'Proceed '.$soup.' monthsales with soup.';
echo PHP_EOL.'Proceed '.$bakes.' monthsales with bakes.';
