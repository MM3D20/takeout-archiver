<?php
require_once 'settings.php';
$db_server = mysqli_connect($db_host, $db_user, $db_pass, $db_name);
$file = fopen('dictionary.json', 'r') or die('File ERROR');
$dictionary = json_decode(fread($file, filesize('dictionary.json')), true);
fclose($file);
function db_query($query)
{
    global $db_server;
    return mysqli_query($db_server, $query);
}
/*  EXCUTE FOLLOWING STATEMENTS IN MYSQL IN ORDER TO CONFIGURE DATABASE

create table foodtype select * from food;

ALTER TABLE `foodtype`
ENGINE = MyISAM ,
ADD COLUMN `internalid` INT(32) UNSIGNED NOT NULL AUTO_INCREMENT FIRST,
ADD COLUMN `status` CHAR(1) NULL AFTER `date`,
ADD COLUMN `foodtype` VARCHAR(1) NULL AFTER `status`,
ADD COLUMN `suittype` MEDIUMTEXT NULL AFTER `foodtype`,
ADD COLUMN `value` CHAR(6) NULL AFTER `suittype`,
ADD PRIMARY KEY (`internalid`);


Food status
    * NULL--Not checked yet
    * 0--INVALID
    * 1--GOOD
    * 2--SUIT_OK(NOT PARSED)
    * 3--BAD(NO RULES MATCH)
    * 4--BAD_SUIT
Food type
    * 0--Bakes
    * 1--Rice
    * 2--Noodles
    * 3--Hamburger/French fries
    * 4--Fried Chicken/duck/fish
    * 5--Dumplings
    * 6--Dishes
    * 7--Drinks
    * 8--Soup
    * 9--OUT OF DICTIONARY
Suit food type
    * ROWS_numArow1Arow2A.....Arown
Food value
    * xxxxxx
    * [0]:Wood(chopsticks) [1]:Plastic Bag [2]:Plastic Bowl [3]:Aluminium fuil [4]:Paper [5]:other
*/
function write_invalid($row_num, $type, $row)
{
    if ($type == 0) {
        $erri = '[Suit]';
    } elseif ($type == 1) {
        $erri = '[Signal]';
    }
    $log = '[NOT PARSED]'.$erri.'  '.$row[0].' : '.$row[3].PHP_EOL;
    file_put_contents('Not_parsed.log', $log, FILE_APPEND);
}
function is_suit($row)
{
    global $dictionary;
    $str = $row[3];
    foreach ($dictionary['suit'] as $match) {
        if (strpos($str, $match)) {
            return true;
        }
    }
    return false;
}
function suit($row, $rown)
{
    global $dictionary;
    for ($i=0; $i<=5; $i++) {
        $metric[] = 0;
    }
    $metric_all = '';
    $type_all = '';
    if (strpos($row[3], '+')) {
        $items = explode('+', $row[3]);
    } elseif (strpos($row[4], '+')) {
        $items = explode('+', $row[4]);
    } else {
        db_query("UPDATE `foodtype` SET `status`='4' WHERE `internalid`='$i'");
        return false;
    }
    $cnt = 0;
    foreach ($items as $single) {
        $result[$cnt] = parse_single_item($single);
        if ($result[$cnt]==9) {
            db_query("UPDATE `foodtype` SET `status`='3' WHERE `internalid`='$i'");
            return false;
        } elseif ($result[$cnt]==10) {
            db_query("UPDATE `foodtype` SET `status`='0' WHERE `internalid`='$i'");
            return true;
        }
        $cnt++;
    }
    if ($cnt <= 1) {
        db_query("UPDATE `foodtype` SET `status`='4' WHERE `internalid`='$i'");
        return false;
    }
    $cnt --;
    for ($i=0; $i <= $cnt; $i++) {
        $type_all .= 'A';
        $type_all .= $result[$i];
        $tmp_type = $result[$i];
        $tmp = str_split($dictionary['metric'][$tmp_type]);
        for ($j=0; $j<=5; $j++) {
            $metric[$j] = (int)$tmp[$j] + $metric[$j];
        }
    }
    for ($i=0; $i<=5; $i++) {
        if ($metric[$i] > 9) {
            $metric[$i] = 9;
            $metric[5] = 1;
        }
    }
    for ($i=0; $i<=5; $i++) {
        $metric_all .= $metric[$i];
    }
    db_query("UPDATE `foodtype` SET `foodtype`=NULL WHERE `internalid`='$rown'");
    db_query("UPDATE `foodtype` SET `suittype`='$type_all' WHERE `internalid`='$rown'");
    db_query("UPDATE `foodtype` SET `value`='$metric_all' WHERE `internalid`='$rown'");
    db_query("UPDATE `foodtype` SET `status`='1' WHERE `internalid`='$rown'");
    return true;
}
function single($row, $rown)
{
    global $dictionary;
    $result = parse_single_item($row[3]);
    if ($result != 9 && $result != 10) {
        $metric = $dictionary['metric'][$result];
        db_query("UPDATE `foodtype` SET `foodtype`='$result' WHERE `internalid`='$rown'");
        db_query("UPDATE `foodtype` SET `value`='$metric' WHERE `internalid`='$rown'");
        db_query("UPDATE `foodtype` SET `status`='1' WHERE `internalid`='$rown'");
        return true;
    } elseif ($result == 10) {
        db_query("UPDATE `foodtype` SET `status`='0' WHERE `internalid`='$rown'");
        return true;
    } else {
        db_query("UPDATE `foodtype` SET `status`='3' WHERE `internalid`='$rown'");
        write_invalid($rown, 1, $row);
        return false;
    }
}
function parse_single_item($str)
{
    global $dictionary;
    foreach ($dictionary['type']['rice'] as $type) {
        if (strpos($str, $type) == true || $str == $type) {
            return 1;
        }
    }
    foreach ($dictionary['type']['noodles'] as $type) {
        if (strpos($str, $type) == true || $str == $type) {
            return 2;
        }
    }
    foreach ($dictionary['type']['hamburger'] as $type) {
        if (strpos($str, $type) == true || $str == $type) {
            return 3;
        }
    }
    foreach ($dictionary['type']['friedmeat'] as $type) {
        if (strpos($str, $type) == true || $str == $type) {
            return 4;
        }
    }
    foreach ($dictionary['type']['dumplings'] as $type) {
        if (strpos($str, $type) == true || $str == $type) {
            return 5;
        }
    }
    foreach ($dictionary['type']['dishes'] as $type) {
        if (strpos($str, $type) == true || $str == $type) {
            return 6;
        }
    }
    foreach ($dictionary['type']['drinks'] as $type) {
        if (strpos($str, $type) == true || $str == $type) {
            return 7;
        }
    }
    foreach ($dictionary['type']['soup'] as $type) {
        if (strpos($str, $type) == true || $str == $type) {
            return 8;
        }
    }
    foreach ($dictionary['type']['bakes'] as $type) {
        if (strpos($str, $type) == true || $str == $type) {
            return 0;
        }
    }
    foreach ($dictionary['type']['invalid'] as $type) {
        if (strpos($str, $type) == true || $str == $type) {
            return 10;
        }
    }
    return 9;
}
$result = mysqli_fetch_row(db_query("SELECT COUNT(*) FROM foodtype"));
$counts = $result[0];
unset($result);
for ($i=1; $i<=$counts; $i++) {
    $result = db_query("SELECT * FROM foodtype WHERE internalid='$i'");
    $row = mysqli_fetch_row($result);
    if ($row[6] != 0 && $row[8] != 1 && $row[8] != 0 || $row[8] == null) {
        if (is_suit($row) == true) {
            db_query("UPDATE `foodtype` SET `status`='2' WHERE `internalid`='$i'");
            if (suit($row, $i) == true) {
                echo '[INFO]'.$row[1].' parsed. Marked as VALID'.PHP_EOL;
            } else {
                echo '[WARN]'.$row[1].' parsed. No rules match'.PHP_EOL;
            }
        } else {
            if (single($row, $i) == true) {
                echo '[INFO]'.$row[1].' parsed. Marked as VALID'.PHP_EOL;
            } else {
                echo '[WARN]'.$row[1].' parsed. No rules match'.PHP_EOL;
            }
        }
    } elseif ($row[6] == 0 && !isset($row[8])) {
        db_query("UPDATE `foodtype` SET `status`='0' WHERE `internalid`='$i'");
        echo '[INFO]'.$row[1].' parsed. Marked as INVALID'.PHP_EOL;
    }
}
