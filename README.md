# Takeout_Archiver

## Description
A project to archive and analyze data from ele.me.

Applied for DengFengbei 2018 Mathematical Modeling Competition Preliminary Match.

Only for 3 Divides 20 Group.

## NOCTICE
Due to the persional information leak, I have to rebuild the git, so no history change available.

## Changelog
You can check the full changelog [here](https://github.com/MM3D20/takeout-archiver/blob/master/CHANGELOG.md)

## Required software
- php7
- mysql

## Usage
```
1. Configure settings.php with correct information(including calculating Geohash)
2. Excute the SQL statements in mysql
3. Excute the script using php xxx.php
```
Sql statements are available [here](https://github.com/MM3D20/takeout-archiver/blob/master/MYSQL.md)
